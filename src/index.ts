import got, { HTTPError } from 'got/dist/source';

const CHECK_INTERVAL = 1000 * 60 * 5;
const API_BASE_URL = 'https://login.rapmlsqa.com/api';
const MLS_ID = 'CIN';

async function main() {
	const accessToken = process.argv[2];
	if (!accessToken) {
		console.error('No access token provided');
		return;
	}
	const now = new Date().toISOString();
	console.log(`[${now}] started`);
	const interval = setInterval(async () => {
		const now = new Date().toISOString();
		console.log(`[${now}] checking token`);
		try {
			await got(`${API_BASE_URL}/${MLS_ID}/Listing/myListings`, {
				headers: {
					Authorization: `Bearer ${accessToken}`,
				},
			});
		} catch (e) {
			if (e instanceof HTTPError && e.response.statusCode === 401) {
				console.log(`[${now}] token expired`);
			} else {
				console.log(`[${now}] error: `, e);
			}
			clearInterval(interval);
		}
	}, CHECK_INTERVAL);
}

main();
